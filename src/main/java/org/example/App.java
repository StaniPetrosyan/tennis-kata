package org.example;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        System.out.println("Generate match");
        Match match = new Match();
        System.out.println(match.getScore());

        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Indicare chi prende il punto: 1 per giocatore a casa, 2 per giocatore fuori casa");
            String choose = in.nextLine();
            switch (choose) {
                case "1": match.playerHome.setPoint();
                    break;
                case "2": match.playerAway.setPoint();
                    break;
            }
            System.out.println(match.getScore());
        } while(!match.getScore().contains("won"));

    }
}
