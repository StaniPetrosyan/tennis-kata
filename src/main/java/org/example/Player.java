package org.example;

import java.util.Arrays;
import java.util.List;

public class Player {
    private static final List<String> scores = Arrays.asList("love", "fifteen", "thirty", "forty");
    private int score;
    private String name;

    public Player(String n) {
        score = 0;
        name = n;
    }

    public void setPoint() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public String getScoreDescription() {
        return scores.get(score);
    }

    public String getName() { return name; }
}
