package org.example;

public class Match {
    public Player playerHome;
    public Player playerAway;

    public Match() {
        playerHome = new Player("player home");
        playerAway = new Player("player away");
    }

    public String getScore() {
        if (hasAtLeastThreePoints()){
            if (hasTwoMorePointsOtherPlayer()) {
                return getLeadPlayer().getName() + " won";
            } else if (playersHaveSamePoints()) {
                return "deuce";
            } else {
                return "advantage";
            }
        } else {
            return playerHome.getScoreDescription() + "-" + playerAway.getScoreDescription();
        }
    }

    private Player getLeadPlayer() {
        return (playerHome.getScore() > playerAway.getScore()) ? playerHome : playerAway;
    }

    private boolean hasAtLeastThreePoints() {
        return playerHome.getScore() >= 3 || playerAway.getScore() >= 3;
    }

    private boolean hasTwoMorePointsOtherPlayer() {
        return Math.abs(playerHome.getScore() - playerAway.getScore()) >= 2;
    }

    private boolean playersHaveSamePoints() {
        return playerHome.getScore() == playerAway.getScore();
    }
}
