package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class MatchTest {

    @Test
    public void ShouldStartWithLoveScore() {
        Match match = new Match();
        assertEquals(match.playerHome.getScoreDescription(), "love");
        assertEquals(match.playerAway.getScoreDescription(), "love");
    }

    @Test
    public void ShouldPlayerOneWin() {
        Match match = new Match();
        for (int i = 0; i < 3; i++) {
            match.playerHome.setPoint();
        }
        for (int i = 0; i < 1; i++) {
            match.playerAway.setPoint();
        }
        assertEquals("player home won", match.getScore());
    }


    @Test
    public void ShouldBeMatchDeuce() {
        Match match = new Match();

        for (int i = 0; i < 3; i++) {
            match.playerHome.setPoint();
        }
        for (int i = 0; i < 3; i++) {
            match.playerAway.setPoint();
        }
        assertEquals("deuce", match.getScore());

    }

    @Test
    public void ShouldPlayerOneInAdvantage(){
        Match match = new Match();

        for (int i = 0; i < 3; i++) {
            match.playerHome.setPoint();
        }
        for (int i = 0; i < 2; i++) {
            match.playerAway.setPoint();
        }
        assertEquals("advantage", match.getScore());
    }

}