package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void ShouldScoreLabeledRightWay() {
        Player player = new Player("player 1");
        String[] score = new String[4];
        for (int i = 0; i < 3; i++) {
            score[i] = player.getScoreDescription();
            player.setPoint();
        }
        score[3] = player.getScoreDescription();

        assertArrayEquals(new String[]{"love", "fifteen", "thirty", "forty"}, score);
    }
}